# Life Points Counter

A web-based life points counter for yu-gi-oh style games, or similar card games.

No tracking, no privacy problems, no subscription, no ads. "Just works".

## Building

The project can be built using rust and dioxus library.

Install dioxus's `dx` tool.
Then run serve for a preview, or build to create the production version:

```dx serve```

```dx build```

## Copyright

In this section you can find copyright and Licensing information you have to comply to in order to use this software.

Copyright © 2023 Massimo Gismondi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
