/*
Copyright © 2023 - Massimo Gismondi

This file is part of LifePoints Counter.

LifePoints Counter is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LifePoints Counter is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LifePoints Counter.  If not, see <http://www.gnu.org/licenses/>.
*/

#![allow(non_snake_case)]
// import the prelude to get access to the `rsx!` macro and the `Scope` and `Element` types
use dioxus::prelude::*;
use web_sys::window;
mod lp_storage;
use lp_storage::LpStorage;

fn main() {
    // launch the web app
    dioxus_web::launch(App);
}

// create a component that renders a div with the text "Hello, world!"
fn App(cx: Scope) -> Element {
    let eval = use_eval(cx.scope);
    //let _ = eval("screen.orientation.lock(\"landscape-primary\")");

    let p1_lp = use_ref(cx, ||{LpStorage::new(0)});
    let p2_lp = use_ref(cx, ||{LpStorage::new(1)});
    
    
    cx.render(rsx! {
        style
        {
            dangerous_inner_html: include_str!("style.css")
        }
        div
        {
            style: "height: 100vh; width: 100vw; display: flex; align-items: center; justify-content:space-around;",
            div
            {
                Counter {player: p1_lp}
            }
            div
            {
                Counter {player: p2_lp}
            }
        }
        ResetButton{player_1: p1_lp, player_2: p2_lp}
    })
}

#[inline_props]
fn Counter<'a>(cx: Scope, player: &'a UseRef<LpStorage>) -> Element
{
    let space_below_lp = match player.read().get_name().len()>0
    {
        true => "margin-bottom: 0px",
        false => ""
    };
    return cx.render(
        rsx!(
            div
            {
                style: "display:flex; flex-direction: column;",
                div
                {
                    class:"button_column_centerer",
                    ButtonCounter{amount: 50, player: player}
                    ButtonCounter{amount: 100, player: player}
                    ButtonCounter{amount: 500, player: player}
                    ButtonCounter{amount: 1000, player: player}
                }
                div
                {
                    class: "lifepoints",
                    h2
                    {
                        style: "text-align:center; {space_below_lp}",
                        class: if player.read().get_lp() <= 0
                        {
                            "player-lost lifepoints"
                        }
                        else {"lifepoints"},
                        "{player.read().get_lp()}"
                    }
                    p {style:"font-size: 20px", "{player.read().get_name()}"}
                }
                div
                {
                    class:"button_column_centerer",
                    ButtonCounter{amount: -1000, player: player}
                    ButtonCounter{amount: -500, player: player}
                    ButtonCounter{amount: -100, player: player}
                    ButtonCounter{amount: -50, player: player}
                }
            }
        )
    )
}

#[inline_props]
fn ButtonCounter<'a>(cx: scope, amount: i32, player: &'a UseRef<LpStorage>) -> Element
{
    cx.render(
        rsx!(
            button
            {
                class: "increment_button",
                // class:"lifepoints",
                onclick: |_|
                {
                    let old_lp = player.read().get_lp();
                    player.write().set_lp(
                        old_lp + *amount
                    )
                },
                if *amount > 0  {
                    rsx!("+{amount}")
                }
                else
                {
                    rsx!("{amount}")
                }
            }
        )
    )
}


#[inline_props]
fn ResetButton<'a>(cx: Scope,
    player_1: &'a UseRef<LpStorage>,
    player_2: &'a UseRef<LpStorage>
) -> Element
{
    let active = use_state(cx, ||{false});

    let local_player_names = [
        use_ref(cx, ||{player_1.read().get_name().to_string()}),
        use_ref(cx, ||{player_2.read().get_name().to_string()})
    ];

    cx.render(
        rsx!(
            button
            {
                class: "reset-button secondary-button",
                onclick: |_|
                {
                    active.set(true);
                },
                "Nuova partita"
            }
            if *active.get()
            {
                rsx!(
                    Popup{
                        title: "",
                        on_click_close: |_|{active.set(false);},
                        form
                        {
                            div
                            {
                                class: "input-group",
                                label {r#for:"g1_id",  "Giocatore 1"}
                                input
                                {
                                    id:"g1_id", required:false, maxlength: 20,
                                    value: "{local_player_names[0].read()}",
                                    onchange: move |e| { local_player_names[0].set( e.value.clone()) }
                                }
                            }
                            div
                            {
                                class: "input-group",
                                label {r#for:"g2_id",  "Giocatore 2"}
                                input {
                                    id:"g2_id", required:false, maxlength: 20,
                                    value: "{local_player_names[1].read()}",
                                    onchange: move |e| { local_player_names[1].set( e.value.clone()) }
                                }
                            }
                            button
                            {
                                r#type: "button",
                                onclick: move |_|{
                                    player_1.write().set_name(
                                        &local_player_names[0].read().clone()
                                    );
                                    player_2.write().set_name(
                                        &local_player_names[1].read().clone()
                                    );
                                    player_1.write().reset_lp();
                                    player_2.write().reset_lp();
                                    active.set(false);
                                },
                                "Avvia partita"
                            }
                        }                        
                    }
                )
            }
        )
    )
}

#[inline_props]
fn Popup<'a>(cx: Scope, on_click_close: EventHandler<'a, ()>, title: &'static str, children: Element<'a>) -> Element
{
    let active_rsx = rsx!(
        div
        {
            // Sfondo annerito
            class:"popup-background",
            div
            {
                class:"popup",
                button
                {
                    style:"margin-top: 3rem",
                    class: "secondary-button",
                    onclick: |_|{
                        on_click_close.call(());
                    },
                    "X"
                }
                if !title.is_empty()
                {
                    rsx!(h1{"{title}"})
                }
                children
            }
        }
    );
    cx.render(
        active_rsx
    )
}
