const STARTING_LP: i32 = 8000;

use web_sys::{Storage};
pub struct LpStorage{
    local_storage: Storage,
    player_index: u8,
    cache_name: String,
    cache_lp: i32
}

impl LpStorage {
    pub fn new(player_index: u8) -> Self {
        let w = web_sys::window().unwrap();
        let local_storage = w.local_storage().unwrap().unwrap();

        let mut s = Self
        {
            local_storage,
            player_index,
            cache_lp: 0,
            cache_name: String::new()
        };
        s.force_load();

        s
    }

    fn force_load(&mut self)
    {
        self.cache_lp = self.local_storage
            .get_item(&self.lp_key())
            .unwrap()
            .map(|v| v.parse().unwrap())
            .unwrap_or(STARTING_LP);
        
        self.cache_name = self.local_storage
            .get_item(&self.name_key())
            .unwrap()
            .unwrap_or_default()
    }

    pub fn get_lp(&self) -> i32
    {
        return self.cache_lp
    }

    pub fn set_lp(&mut self, lp: i32)
    {
        self.local_storage.set_item(
            &self.lp_key(),
            &lp.to_string()
        ).unwrap();

        self.cache_lp = lp;
    }

    pub fn reset_lp(&mut self)
    {
        self.set_lp(STARTING_LP);
    }    

    pub fn get_name<'a>(&'a self) -> &'a str
    {
        &self.cache_name
    }

    pub fn set_name(&mut self, name: &str)
    {
        self.local_storage.set_item(
            &self.name_key(),
            name
        ).unwrap();

        self.cache_name = String::from(name);
    }


}

impl LpStorage
{
    fn name_key(&self) -> String
    {
        format!("{}_name", self.player_index)
    }
    fn lp_key(&self) -> String
    {
        format!("{}_lp", self.player_index)
    }
}